# WSL2 编译 rt-thread 工程

## 安装软件

```sh
sudo apt install scons
sudo apt install gcc-arm-none-eabi
```

将 stlink 和 plink 工具移动到 WSL2 中，并设置软链接到 `/usr/bin` 目录
