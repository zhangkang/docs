# 第一级标题
## 第二级标题
### 第三级标题

```html

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="description" content="Description">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify@4/lib/themes/vue.css">
</head>
<body>
  <div id="app">加载中</div>
  <script>
    window.$docsify = {
      name: 'kang',
      nameLink: '/',
      repo: 'docsifyjs/docsify',
      loadSidebar: true,    //侧边导航栏
      subMaxLevel: 2,       //侧边显示几级，一级不显示
      loadNavbar: true,     //顶部导航栏
      coverpage: true,      //封面
      onlyCover: true,      //只显示封面
      auto2top: true,       //切换页面时跳转到顶部
      relativePath: true,   //启用相对路径
      alias: {
        // '/docsify/README': '/README',
        // '/kang/README': '/docsify/README',
      },
    }
  </script>
  <!-- Docsify v4 -->
  <script src="//cdn.jsdelivr.net/npm/docsify@4"></script>
  <script src="//cdn.jsdelivr.net/npm/docsify-copy-code/dist/docsify-copy-code.min.js"></script>
</body>
</html>

```
