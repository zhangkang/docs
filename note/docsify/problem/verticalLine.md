# docsify 中 MarkDown表格使用竖线

今天在使用 MarkDown 表格中使用 `|` 时出了问题

可以发现直接使用 `|` 会出现以下问题

```text
VERTICAL|CYCLE
```

| 示例             |      |
| ---------------- | ---- |
| VERTICAL|CYCLE |      |

### 方案1

```text
VERTICAL\|CYCLE
```

| 示例            |      |
| --------------- | ---- |
| VERTICAL\|CYCLE |      |

### 方案2

```text
VERTICAL&#124;CYCLE
```

| 示例                |      |
| ------------------- | ---- |
| VERTICAL&#124;CYCLE |      |

当然，如果想使用标记行内代码的话就不能随便使用上面两种解决方案了

### 方案1

```text
`VERTICAL\|CYCLE`
```

| 示例              |      |
| ----------------- | ---- |
| `VERTICAL\|CYCLE` |      |

### 方案2

```text
<code>VERTICAL&#124;CYCLE</code>
```

| 示例                             |      |
| -------------------------------- | ---- |
| <code>VERTICAL&#124;CYCLE</code> |      |
