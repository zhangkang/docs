# WSL2 安装

[WSL2 安装官方网站](https://docs.microsoft.com/zh-cn/windows/wsl/install-manual)

```bash
WSL2 请启用虚拟机平台 Windows 功能并确保在 BIOS 中启用虚拟化
```

需要启用Hyper-V

```bash
WSL 2 需要更新其内核组件。有关信息，请访问 https://aka.ms/wsl2kernel
```

如果出现以上报错，下载安装即可
![image-20220208155424200](figures/image-20220208155424200.png)

[适用于 x64 计算机的 WSL2 Linux 内核更新包](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)

```bash
wsl --set-version Ubuntu-20.04  2 #转化Ubuntu-20.04为WSL2
```

```bash
wsl -l -v #查看wsl版本
```
