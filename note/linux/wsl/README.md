# WSL

这里介绍了一些关于 WSL 的一些用法

- [WSL2 安装](/note/linux/wsl/install.md)
- [WSL2 迁移](/note/linux/wsl/move.md)
- [WSL2 安装 Arch](/note/linux/wsl/arch.md)
