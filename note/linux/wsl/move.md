# WSL2 迁移

## 迁移

这里我们使用到 export 、unregister 和 import 几个选项就可以实现迁移。

1. 终止正在运行的分发或虚拟机：

   ```PowerShell
   wsl --shutdown
   ```

2. 对需要迁移的分发或虚拟机导出（我安装的版本是Ubuntu-20.04）：

   ```PowerShell
   wsl --export Ubuntu-20.04 D:\wsl-Ubuntu-20.04
   ```

3. 卸载分发版或虚拟机（如果是要重装系统或换机器安装，这一步可以省略，但是要将上一步导出的文件保存好）

   ```PowerShell
   wsl --unregister Ubuntu-20.04
   ```

4. 导入新的分发版或虚拟机：

    按照默认版本导入

   ```PowerShell
   wsl --import Ubuntu-20.04 D:\wsl\Ubuntu2004 D:\wsl-Ubuntu-20.04
   ```

   还可以在最后添加参数 `--version 2` 以特定的版本导入

## 修改默认用户

在我们迁移之后，会默认按照 `root` 用户登录
可以使用下面路径下的 `ubuntu` 开头的 `exe` 文件来修改默认用户

```PowerShell
C:\Users\你的Windows用户名\AppData\Local\Microsoft\WindowsApps
```

比如：我的是 `ubuntu2004.exe`

运行如下指令来修改 Ubuntu 默认用户

```PowerShell
ubuntu2004 config --default-user 你的用户
```

## 查看命令

在 Windows 的 PowerShell 中输入:

```PowerShell
wsl --help
```

可以看到关于这个命令的使用帮助说明：

```PowerShell
用法: wsl.exe [Argument] [Options...] [CommandLine]

运行 Linux 二进制文件的参数:

    如果未提供命令行，wsl.exe 将启动默认 shell。

    --exec, -e <CommandLine>
        在不使用默认 Linux Shell 的情况下执行指定的命令。

    --
        按原样传递其余命令行。

选项:
    --cd <Directory>
        将指定目录设置为当前工作目录。
        如果使用了 ~，则将使用 Linux 用户的主页路径。如果路径
        以 / 字符开头，将被解释为绝对 Linux 路径。
        否则，该值一定是绝对 Windows 路径。

    --distribution, -d <Distro>
        运行指定分发。

    --user, -u <UserName>
        以指定用户身份运行。

管理适用于 Linux 的 Windows 子系统的参数:

    --help
        显示用法信息。

    --install [选项]
        安装额外的适用于 Linux 的 Windows 子系统分发。
         要获得有效分发列表，请使用“wsl --list --online”。

        选项:
            --distribution, -d [参数]
                按名称下载并安装分发。

                参数:
                    有效分发名称(不区分大小写)。

                示例:
                    wsl --install -d Ubuntu
                    wsl --install --distribution Debian

    --set-default-version <Version>
        更改新分发的默认安装版本。

      --shutdown
         立即终止所有运行的分发及 WSL 2
        轻型工具虚拟机。

         --status
           显示适用于 Linux 的 Windows 子系统的状态。

    --update [选项]
        如果未指定任何选项，则 WSL 2 内核将更新
        到最新版本。

             选项:
         --rollback
                还原到 WSL 2 内核的先前版本。

用于管理适用于 Linux 的 Windows 子系统中的分发的参数:

    --export <Distro> <FileName>
         将分发导出到 tar 文件。
        对于标准输出，文件名可以是 –。

    --import <Distro> <InstallLocation> <FileName> [Options]
            将指定的 tar 文件作为新分发导入。
          对于标准输入，文件名可以是 –。

        选项:
            --version <Version>
                指定要用于新分发的版本。

    --list, -l [Options]
        列出分发。
        选项:
            --all
                列出所有分发，包括
        当前正在安装或卸载的分发。

            --running
                仅列出当前正在运行的分发。

            --quiet, -q
                仅显示分发名称。

            --verbose, -v
                显示所有分发的详细信息。

            --online, -o
                显示使用“wsl --install”进行安装的可用分发列表。

    --set-default, -s <分发>
        将分发设置为默认值。

    --set-version <分发> <版本>
        更改指定分发的版本。

    --terminate, -t <分发>
        终止指定的分发。

    --unregister <分发>
        注销分发并删除根文件系统。
```
