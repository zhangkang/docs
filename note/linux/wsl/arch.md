# WSL2 安装 Arch

## 安装 LxRunOffline

[LxRunOffline](https://github.com/DDoSolitary/LxRunOffline/releases)

## 下载 ArchLinux

[清华大学镜像 ArchLinux](https://mirrors.tuna.tsinghua.edu.cn/archlinux/iso/latest/)

## 安装 ArchLinux 到 WSL

```powershell
LxRunOffline i -n <自定义名称> -f <Arch镜像位置> -d <安装系统的位置> -r root.x86_64
```

```powershell
.\LxRunOffline.exe i -n ArchLinux -f D:\archlinux-bootstrap-2022.04.05-x86_64.tar.gz -d D:\wsl\ArchLinux -r root.x86_64
```

转化为 WSL2

```powershell
wsl --set-version ArchLinux 2
```

## 首次进入系统

```powershell
wsl -d <名字>
```

```powershell
wsl -d ArchLinux
```

```bash
rm /etc/resolv.conf
exit
```

## 再次进入系统

```powershell
wsl --shutdown ArchLinux
wsl -d ArchLinux
```

## Arch Linux 软件仓库镜像使用帮助

编辑 `/etc/pacman.d/mirrorlist` 在文件的最顶端添加

```bash
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinux/$repo/os/$arch
```

## ArchlinuxCN 镜像

`/etc/pacman.conf`

```bash
[archlinuxcn]
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
```

```bash
pacman -Syy
pacman-key --init
pacman-key --populate
pacman -S archlinuxcn-keyring
pacman -S base base-devel vim git wget
```

```bash
passwd
```

```bash
useradd -m -G wheel -s /bin/bash <用户名>
passwd <用户名>
```

```bash
vim /etc/sudoers
```

将文件/etc/sudoers中的wheel ALL=(ALL) ALL那一行前面的注释去掉

```bash
id -u <用户名>
exit
```

```powershell
.\LxRunOffline.exe su -n <你的arch名字> -v <账户id>
```
