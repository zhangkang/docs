# Ubuntu 换源

## 一、查看版本

修改源之前提示大家先查看版本名，输入

```sh
lsb_release -c
```

会显示 `Codename:`

ubuntu20.04对应的是 `focal`

## 二、备份源文件

输入

```sh
sudo cp -v /etc/apt/sources.list /etc/apt/sources.list.backup
```

## 三、修改源文件

```sh
sudo vim /etc/apt/sources.list
```

## 四、更新源

分别输入如下两个代码

1. 更新源

    ```sh
    sudo apt-get update
    ```

2. 更新软件：

    ```sh
    sudo apt-get upgrade
    ```

3. 另：（如出现依赖问题，输入如下：）

    ```sh
    sudo apt-get -f install
    ```

## 附：各种源（20.04版本）

如果是其他版本的，将一中查看到的版本信息替换下面的 `focal`，即可。

### 阿里云源

```sh
 
deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse

```
