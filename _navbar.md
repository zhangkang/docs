<!-- _navbar.md -->
- [首页](/home/)
- [笔记](/note/)
  - [docsify](/note/docsify/)
  - [Linux](/note/linux/)
- [工具](/tools/)
  - [Everything](/tools/Everything/)
  - [utools](/tools/utools/)
  - [powertoys](/tools/powertoys/)
